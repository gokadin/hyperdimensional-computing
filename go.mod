module github.com/gokadin/hyperdimensional-computing

go 1.12

require (
	github.com/gosuri/uilive v0.0.3 // indirect
	github.com/gosuri/uiprogress v0.0.1
	github.com/mattn/go-isatty v0.0.8 // indirect
)
